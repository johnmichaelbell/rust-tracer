use rayon::prelude::*;
use ron;
use serde::Deserialize;
use structopt::StructOpt;
use ultraviolet as uv;

use std::{f32, fs, path, time};

fn main() {
    let opts = Opts::from_args();
    let scene_file = fs::File::open(&opts.scene).expect("File not found.");
    let scene: Scene = ron::de::from_reader(scene_file).unwrap();

    render(&scene.spheres, &scene.lights, &opts);
}

#[derive(StructOpt)]
/// Generate an image of a scene using ray tracing
struct Opts {
    #[structopt(long = "width", short = "w", default_value = "1024")]
    width: usize,

    #[structopt(long = "height", short = "h", default_value = "768")]
    height: usize,

    #[structopt(long = "fov", short = "f", default_value = "1.0471975512")]
    fov: f32,

    #[structopt(long = "scene", short = "s", default_value = "./scene.ron")]
    scene: path::PathBuf,
}

#[derive(Deserialize)]
struct Scene {
    spheres: Vec<Sphere>,
    lights: Vec<Light>,
}

fn render(spheres: &[Sphere], lights: &[Light], opts: &Opts) {
    let begin = time::Instant::now();
    let framebuffer = (0..opts.height)
        .into_par_iter()
        .flat_map(|j| (0..opts.width).into_par_iter().map(move |i| (i as f32, j as f32)))
        .map(|(i, j)| {
            let (width, height) = (opts.width as f32, opts.height as f32);
            let dir_x = (i + 0.5) - width / 2.;
            let dir_y = -(j + 0.5) + height / 2.;
            let dir_z = -height / (2. * f32::tan(opts.fov / 2.));
            cast_ray(
                &uv::Vec3::zero(),
                &uv::Vec3::new(dir_x, dir_y, dir_z).normalized(),
                &spheres,
                &lights,
                0,
            )
        })
        .collect::<Vec<_>>();

    dbg!(begin.elapsed());
    let begin = time::Instant::now();
    use std::fs::File;
    use std::io::Write;
    let mut f = File::create("./out.ppm").unwrap();
    write!(&mut f, "P6\n{} {}\n255\n", opts.width, opts.height).unwrap();
    let buffer = framebuffer
        .iter()
        .flat_map(|pixel| pixel.as_array().iter())
        .map(|&pixel| (255. * pixel).max(0.).min(255.) as u8)
        .collect::<Vec<_>>();
    f.write_all(&buffer[..]).unwrap();

    dbg!(begin.elapsed());
}

struct Hit<'a> {
    point: uv::Vec3,
    normal: uv::Vec3,
    material: &'a Material,
}

fn scene_intersect<'a>(orig: &uv::Vec3, dir: &uv::Vec3, spheres: &'a [Sphere]) -> Option<Hit<'a>> {
    let mut spheres_dist = f32::MAX;
    let mut hit = None;
    for sphere in spheres.iter() {
        let mut dist_i = 0.;
        if sphere.ray_intersect(orig, dir, &mut dist_i) && dist_i < spheres_dist {
            spheres_dist = dist_i;
            let point = *orig + *dir * dist_i;
            let normal = (point - uv::Vec3::from(sphere.center)).normalized();
            let material = &sphere.material;
            hit = Some(Hit { point, normal, material });
        }
    }
    hit
}

fn cast_ray(
    orig: &uv::Vec3,
    dir: &uv::Vec3,
    spheres: &[Sphere],
    lights: &[Light],
    depth: usize,
) -> uv::Vec3 {
    if depth < 4 {
        if let Some(hit) = scene_intersect(orig, dir, spheres) {
            let reflect_dir = dir.reflected(hit.normal).normalized();
            let refract_dir = if dir.dot(hit.normal) < 0. {
                dir.refracted(hit.normal, 1. / hit.material.refractive_index).normalized()
            } else {
                dir.refracted(-hit.normal, hit.material.refractive_index).normalized()
            };
            let reflect_orig = hit.point
                + hit.normal * if reflect_dir.dot(hit.normal) >= 0. { 1e-3 } else { -1e-3 };
            let refract_orig = hit.point
                + hit.normal * if refract_dir.dot(hit.normal) >= 0. { 1e-3 } else { -1e-3 };
            let reflect_color = cast_ray(&reflect_orig, &reflect_dir, spheres, lights, depth + 1);
            let refract_color = cast_ray(&refract_orig, &refract_dir, spheres, lights, depth + 1);

            let (diffuse_light_intensity, specular_light_intensity) =
                lights.iter().fold((0., 0.), |(diffuse, specular), light| {
                    let light_pos = uv::Vec3::from(light.position);
                    let light_dir = (light_pos - hit.point).normalized();
                    let light_distance = (light_pos - hit.point).mag();

                    let shadow_orig = hit.point
                        + hit.normal * if light_dir.dot(hit.normal) >= 0. { 1e-3 } else { -1e-3 };
                    if let Some(shadow_hit) = scene_intersect(&shadow_orig, &light_dir, spheres) {
                        if (shadow_hit.point - shadow_orig).mag() < light_distance {
                            return (diffuse, specular);
                        }
                    }

                    (
                        diffuse + light.intensity * f32::max(0., light_dir.dot(hit.normal)),
                        specular
                            + f32::powf(
                                f32::max(0., (-light_dir).reflected(hit.normal).dot(*dir)),
                                hit.material.specular_exponent,
                            ) * light.intensity,
                    )
                });
            let diffuse_color = uv::Vec3::from(hit.material.diffuse_color);
            return diffuse_color * diffuse_light_intensity * hit.material.albedo[0]
                + uv::Vec3::new(1., 1., 1.) * specular_light_intensity * hit.material.albedo[1]
                + reflect_color * hit.material.albedo[2]
                + refract_color * hit.material.albedo[3];
        }
    }

    uv::Vec3::new(0.2, 0.7, 0.8)
}

#[derive(Debug, Deserialize)]
struct Light {
    position: [f32; 3],
    intensity: f32,
}

#[derive(Debug, Deserialize)]
struct Material {
    refractive_index: f32,
    albedo: [f32; 4],
    diffuse_color: [f32; 3],
    specular_exponent: f32,
}

#[derive(Debug, Deserialize)]
struct Sphere {
    center: [f32; 3],
    radius: f32,
    material: Material,
}

impl Sphere {
    fn ray_intersect(&self, orig: &uv::Vec3, dir: &uv::Vec3, t0: &mut f32) -> bool {
        let l = uv::Vec3::from(self.center) - *orig;
        let tca = l.dot(*dir);
        let d2 = l.dot(l) - tca * tca;
        if d2 > self.radius * self.radius {
            return false;
        };
        let thc = (self.radius * self.radius - d2).sqrt();
        *t0 = tca - thc;
        let t1 = tca + thc;
        if *t0 < 0. {
            *t0 = t1
        }
        *t0 >= 0.
    }
}
